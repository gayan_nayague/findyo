<div class="comment-block">
    <!--parentt comment-->
    <ul class="comment-parent">
        <li>
            <div class="">
                <div class="post-options-btn-block">
                    <div class="post-options-btn">
                        <i class="fa fa-circle-o" aria-hidden="true"></i>
                        <i class="fa fa-circle-o" aria-hidden="true"></i>
                        <i class="fa fa-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="commenter-detail-block">
                    <div class="user-block">
                        <div class="user-img-block">
                            <div class="user-img" style="background-image: url(<?php echo $base_url; ?>/uploads/sample/pro_pic_01.png);"></div>
                        </div>
                        <div class="user-post-right">
                            <div class="user-published-block">
                                <p><a href="#">Pasindu silva</a></p>
                            </div>
                            <div class="user-published-on-block">
                                <p class="post-publish-time-block"><span class="post-publish-time">23 mins</span> ago</p>
                                <!--<p class="post-expire-time-block">This post will expire after <span class="post-expire-time">4 days 8 hours and 06 mins</span></p>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="commenter-msg-block">
                    <div class="comment-holder">
                        <p class="comment">Hi, I want your service but the cost is to much.<br/>Would you please reduce it?</p>
                    </div>
                    <div class="negotiation-block">
                        <input class="negotiation-value" placeholder="Negotiate here" type="text"/>
                    </div>
                </div>
                <div class="comment-react-block">
                    <div class="post-like">
                        <div class="post-like-count">14</div>
                        <img class="post-like-img-hollow hide" src="<?php echo $base_url; ?>/assets/img/icons/like-hollow.png">
                        <img class="post-like-img-filled" src="<?php echo $base_url; ?>/assets/img/icons/like-filled.png">
                    </div>
                    <div class="post-reply">
                        <p>Reply</p>
                    </div>
                </div>
            </div>
            <!--child comment-->
            <div>
                <ul class="comment-child">
                    <li>
                        <div class="post-options-btn-block">
                            <div class="post-options-btn">
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="commenter-detail-block">
                            <div class="user-block">
                                <div class="user-img-block">
                                    <div class="user-img" style="background-image: url(<?php echo $base_url; ?>/uploads/sample/gayan.jpg);"></div>
                                </div>
                                <div class="user-post-right">
                                    <div class="user-published-block">
                                        <p><a href="#">Gayan sandamal (you)</a></p>
                                    </div>
                                    <div class="user-published-on-block">
                                        <p class="post-publish-time-block"><span class="post-publish-time">45 mins</span> ago</p>
                                        <!--<p class="post-expire-time-block">This post will expire after <span class="post-expire-time">4 days 8 hours and 06 mins</span></p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="commenter-msg-block">
                            <div class="comment-holder">
                                <p class="comment">Hi <a class="user-mention" href="#">Pasindu</a>,<br/>I’m sory inform that I’m comfortable with reducing the price</p>
                            </div>
                            <!--                            <div class="negotiation-block">
                                                            <input class="negotiation-value" placeholder="Negotiate here" type="text"/>
                                                        </div>-->
                        </div>
                        <div class="comment-react-block">
                            <div class="post-like">
                                <div class="post-like-count">14</div>
                                <img class="post-like-img-hollow hide" src="<?php echo $base_url; ?>/assets/img/icons/like-hollow.png">
                                <img class="post-like-img-filled" src="<?php echo $base_url; ?>/assets/img/icons/like-filled.png">
                            </div>
                            <div class="post-reply">
                                <p>Reply</p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="post-options-btn-block">
                            <div class="post-options-btn">
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="commenter-detail-block">
                            <div class="user-block">
                                <div class="user-img-block">
                                    <div class="user-img" style="background-image: url(<?php echo $base_url; ?>/uploads/sample/pro_pic_01.png);"></div>
                                </div>
                                <div class="user-post-right">
                                    <div class="user-published-block">
                                        <p><a href="#">Pasindu silva</a></p>
                                    </div>
                                    <div class="user-published-on-block">
                                        <p class="post-publish-time-block"><span class="post-publish-time">1 hour</span> ago</p>
                                        <!--<p class="post-expire-time-block">This post will expire after <span class="post-expire-time">4 days 8 hours and 06 mins</span></p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="commenter-msg-block">
                            <div class="comment-holder">
                                <p class="comment">Ok,<br/>Thanks.</p>
                            </div>
                            <!--                            <div class="negotiation-block">
                                                            <input class="negotiation-value" placeholder="Negotiate here" type="text"/>
                                                        </div>-->
                        </div>
                        <div class="comment-react-block">
                            <div class="post-like">
                                <div class="post-like-count">14</div>
                                <img class="post-like-img-hollow hide" src="<?php echo $base_url; ?>/assets/img/icons/like-hollow.png">
                                <img class="post-like-img-filled" src="<?php echo $base_url; ?>/assets/img/icons/like-filled.png">
                            </div>
                            <div class="post-reply">
                                <p>Reply</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!--child comment-->
        </li>

        <!--reply-->
        <div class="reply-now-block">
            <div class="user-img-block">
                <div class="user-img" style="background-image: url(<?php echo $base_url; ?>/uploads/sample/gayan.jpg);"></div>
            </div>
            <div class="reply-now-message-block">
                <div class="reply-attach-image-block">
                    <img class="reply-attach-image" src="<?php echo $base_url; ?>/assets/img/icons/camera.png"/>
                </div>
                <textarea class="reply-now-message" placeholder="Reply here"></textarea>
            </div>
        </div>
        <!--reply-->
    </ul>
    <!--parentt comment-->

    <!--view more comments-->
    <div class="view-more-comments-block">
        <a href="#">View more comments <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
    </div>
    <!--view more comments-->
</div>