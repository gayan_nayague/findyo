<div class="white-box" id="timeline_filter_card">
    <div class="filter-block">
        <i class="fa fa-search" aria-hidden="true"></i>
        <input class="filter-field" type="text" placeholder="Search/ Filter Timeline"/>
        <span class="filter-by-toggle">Filter by <i class="fa fa-chevron-down" aria-hidden="true"></i></span>
        <button type="button" class="filter-button">Apply</button>
    </div>
    <div class="filter-items-block hide">
        <ul>
            <li class="active">Date
                <span class="orderby-icon-block">
                    <i class="fa fa-sort-amount-asc asc" aria-hidden="true"></i>
                    <i class="fa fa-sort-amount-desc desc hide" aria-hidden="true"></i>
                </span>
            </li>
            <li class="">Time
                <span class="orderby-icon-block">
                    <i class="fa fa-sort-amount-asc asc" aria-hidden="true"></i>
                    <i class="fa fa-sort-amount-desc desc hide" aria-hidden="true"></i>
                </span>
            </li>
            <li class="">Name
                <span class="orderby-icon-block">
                    <i class="fa fa-sort-amount-asc asc" aria-hidden="true"></i>
                    <i class="fa fa-sort-amount-desc desc hide" aria-hidden="true"></i>
                </span>
            </li>
            <li class="">Rate
                <span class="orderby-icon-block">
                    <i class="fa fa-sort-amount-asc asc" aria-hidden="true"></i>
                    <i class="fa fa-sort-amount-desc desc hide" aria-hidden="true"></i>
                </span>
            </li>
        </ul>
    </div>
</div>