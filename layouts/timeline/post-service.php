<div class="white-box post" id="post-user_id-yy-mm-dd-post_no">
    <div class="post-options-btn-block">
        <div class="post-options-btn">
            <i class="fa fa-circle-o" aria-hidden="true"></i>
            <i class="fa fa-circle-o" aria-hidden="true"></i>
            <i class="fa fa-circle-o" aria-hidden="true"></i>
        </div>
    </div>
    <div class="post-top">
        <div class="user-block">
            <div class="user-img-block">
                <div class="user-img" style="background-image: url(<?php echo $base_url; ?>/uploads/sample/gayan.jpg);"></div>
            </div>
            <div class="user-post-right">
                <div class="user-published-block">
                    <p><a href="#">gayan sandamal</a> published <span>a service</span></p>
                </div>
                <div class="user-published-on-block">
                    <p class="post-publish-time-block"><span class="post-publish-time">1 day</span> ago</p>
                    <!--<p class="post-expire-time-block">This post will expire after <span class="post-expire-time">4 days 8 hours and 06 mins</span></p>-->
                </div>
            </div>
        </div>

        <div class="post-content-block">
            <div class="post-content">
                <div class="post-service">
                    <div class="post-service-message">
                        <div class="post-service-text-block">
                            <p class="post-service-description-static">I will <span class="post-service-description-dynamic">design your website</span></p>
                            <p class="post-service-price-block">Starting at <span class="post-service-price">Rs.24,000</span></p>
                        </div>
                        <div class="post-service-buy-button-block">
                            <a class="post-service-buy-button material-btn-click-md">Buy</a>
                        </div>
                    </div>

                    <div class="post-service-content">
                        <div class="post-service-zoom-trigger">
                            <img class="post-service-zoom-trigger-img" src="<?php echo $base_url; ?>/assets/img/icons/enlarge.png"/>
                        </div>

                        <img class="post-service-img" src="<?php echo $base_url; ?>/uploads/sample/interior-3col-render-1.jpg"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="post-controller-block">
            <div class="post-like-block">
                <div class="post-like">
                    <div class="post-like-count">14</div>
                    <img class="post-like-img-hollow" src="<?php echo $base_url; ?>/assets/img/icons/like-hollow.png"/>
                    <img class="post-like-img-filled hide" src="<?php echo $base_url; ?>/assets/img/icons/like-filled.png"/>
                </div>
                <div class="post-liked-users-block">
                    <ul class="post-liked-users-img-block">
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_01.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_02.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_03.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_01.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_02.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_03.png"/>
                        </li>
                    </ul>
                    <div class="post-who-liked">
                        <p>
                            <a class="you-liked" href="#">You</a>
                            <a class="user-liked" href="#">pasindu erange silva</a>
                        <div class="other-liked"><span class="other-liked-count">6</span> more liked this</div>
                        </p>
                    </div>
                </div>
                <div class="post-like-comment-block">
                    <div class="post-comment-block">
                        <div class="post-comment">
                            <div class="post-comment-count">99+</div>
                            <img class="post-comment-icon" src="<?php echo $base_url; ?>/assets/img/icons/comment.png"/>
                        </div>
                    </div>
                    <div class="post-share-block">
                        <div class="post-share">
                            <div class="post-share-count">12</div>
                            <img class="post-share-icon" src="<?php echo $base_url; ?>/assets/img/icons/share.png"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php include 'layouts/timeline/post-service-comment.php' ?>
</div>