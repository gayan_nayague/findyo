<div class="white-box post" id="post-user_id-yy-mm-dd-post_no">
    <div class="post-options-btn-block">
        <div class="post-options-btn">
            <i class="fa fa-circle-o" aria-hidden="true"></i>
            <i class="fa fa-circle-o" aria-hidden="true"></i>
            <i class="fa fa-circle-o" aria-hidden="true"></i>
        </div>
    </div>
    <div class="post-top">
        <div class="user-block">
            <div class="user-img-block">
                <div class="user-img" style="background-image: url(<?php echo $base_url; ?>/uploads/sample/gayan.jpg);"></div>
            </div>
            <div class="user-post-right">
                <div class="user-published-block">
                    <p><a href="#">gayan sandamal</a> published <span>a link</span></p>
                </div>
                <div class="user-published-on-block">
                    <p class="post-publish-time-block"><span class="post-publish-time">16 hours</span> ago</p>
                    <!--<p class="post-expire-time-block">This post will expire after <span class="post-expire-time">4 days 8 hours and 06 mins</span></p>-->
                </div>
            </div>
        </div>

        <div class="post-content-block">
            <div class="post-content">
                <div class="post-link">
                    <div class="post-link-content">
                        <div class="post-link-thumbnail-block">
                            <a href="#" class="post-link">
                                <div class="post-link-thimbnail-overlay">
                                    <img class="video-play-thumbnail-icon" src="<?php echo $base_url; ?>/assets/img/icons/play-video.png"/>
                                </div>
                                <img class="post-link-thimbnail" src="<?php echo $base_url; ?>/uploads/sample/hqdefault.jpg"/>
                            </a>
                        </div>
                        <div class="post-link-title-block">
                            <div class="post-link-title">
                                <h3>How to Make a Website - Web Design Tutorial</h3>
                            </div>
                            <div class="post-link-reference">
                                <p><a href="#">youtube.com</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="post-controller-block">
            <div class="post-like-block">
                <div class="post-like">
                    <div class="post-like-count">14</div>
                    <img class="post-like-img-hollow" src="<?php echo $base_url; ?>/assets/img/icons/like-hollow.png"/>
                    <img class="post-like-img-filled hide" src="<?php echo $base_url; ?>/assets/img/icons/like-filled.png"/>
                </div>
                <div class="post-liked-users-block">
                    <ul class="post-liked-users-img-block">
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_01.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_02.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_03.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_01.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_02.png"/>
                        </li>
                        <li>
                            <img class="post-liked-users-img" src="<?php echo $base_url; ?>/uploads/sample/pro_pic_03.png"/>
                        </li>
                    </ul>
                    <div class="post-who-liked">
                        <p>
                            <a class="you-liked" href="#">You</a>
                            <a class="user-liked" href="#">pasindu erange silva</a>
                        <div class="other-liked"><span class="other-liked-count">6</span> more liked this</div>
                        </p>
                    </div>
                </div>
                <div class="post-like-comment-block">
                    <div class="post-comment-block">
                        <div class="post-comment">
                            <div class="post-comment-count">99+</div>
                            <img class="post-comment-icon" src="<?php echo $base_url; ?>/assets/img/icons/comment.png"/>
                        </div>
                    </div>
                    <div class="post-share-block">
                        <div class="post-share">
                            <div class="post-share-count">12</div>
                            <img class="post-share-icon" src="<?php echo $base_url; ?>/assets/img/icons/share.png"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>