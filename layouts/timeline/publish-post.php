<div class="white-box" id="timeline_publish_card">
    <div class="publish-top">
        <div class="publish-user-img" style="background-image: url(http://localhost/findyo/uploads/sample/gayan.jpg);"></div>
        <ul class="publish-type">
            <li>publish</li>
            <li><a href="#">a post</a></li>
            <li><a href="#">a service</a></li>
            <li><a href="#">an offer</a></li>
            <li><a href="#">an achievement</a></li>
        </ul>
    </div>
    <div class="publish-type">
        <textarea class="publish-write" placeholder="Write now and share your thoughts..."></textarea>
    </div>
    <div class="publish-bottom">
        <div class="timer-check-block">
            <div class="checkbox-holder">
                <input type="checkbox" id="checkbox-1-1" class="ui-checkbox"/>
                <label for="checkbox-1-1"></label>
            </div>
            <div class="checkbox-content-holder">
                <div class="checkbox-text">Make this temporary</div>
                <div class="checkbox-text">for <span class="post-date">7d : 5h : 23m : 21s</span></div>
            </div>
        </div>
        <div class="post-button-block">
            <button class="timer-btn material-btn-click-md">Set timer <i class="fa fa-clock-o" aria-hidden="true"></i></button>
            <button class="post-btn material-btn-click-md">Send <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        </div>
    </div>
</div>