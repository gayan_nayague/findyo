<?php include_once 'profile/profile-banner.php'; ?>

<div class="container no-side-paddings profile-body-block">
    <?php include_once 'sidebar_left.php'; ?>
    <?php include_once 'timeline.php'; ?>
    <?php include_once 'sidebar_right.php'; ?>
</div>