<div class="container no-side-paddings profile_container">
    <!--top selling categories begins-->
    <div class="col-sm-12 no-side-paddings white-box profile_banner" id="profile_banner" style="background-image: url(<?php echo $base_url; ?>/assets/img/banner_image.jpg);">
        <div class="banner-user-status" style="
    background-image: url(<?php echo $base_url; ?>/assets/img/banner_image.jpg);">
            <ul class="banner-user-status-holder">
                <li>Overall rating: <span class="overall-rating-value">87%</span></li>
                <li>Average response time: <span class="avg-response-time-value">15mins</span></li>
                <li>Successful projects/tasks: <span class="successful-tasks-value">211</span></li>
                <li>Recommendations: <span class="recommendations-value">18</span></li>
                <li>Member since: <span class="successful-tasks-value">Jun 2017</span></li>
            </ul>
        </div>

        <div class="banner-controller-block">
            <div class="banner-controller-shade"></div>
            <div class="banner-controller-holder">
                <div class="banner-controller-inner-block one">
                    <ul>
                        <li><a href="#" class="active">Timeline</a></li>
                        <li><a href="#" class="">About</a></li>
                        <li><a href="#" class="">Portfolio</a></li>
                    </ul>
                </div>
                <div class="banner-user-block">
                    <div class="banner-user-image-block" style="background-image: url(<?php echo $base_url; ?>/uploads/sample/gayan.jpg);">
                        <img class="user-flag" src="<?php echo $base_url; ?>/assets/img/sl-flag.png"/>
                    </div>
                    <div class="banner-username-block">
                        <p class="banner-username">gayan sandamal</p>
                    </div>
                    <div class="banner-usertitle-block">
                        <p class="banner-usertitle">UI/ UX Engineer</p>
                    </div>
                </div>
                <div class="banner-controller-inner-block two">
                    <ul>
                        <li><a href="#" class="">Services</a></li>
                        <li><a href="#" class="">Contacts</a></li>
                        <li>
                            <a href="#" class="">
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                                <i class="fa fa-circle-o" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--top selling categories begins-->
</div>