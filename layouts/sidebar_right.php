<div class="profle-right">
    <div class="white-box" id="gallery_card">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title">Gallery</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--sidebar content holder begins-->
        <div class="sidebar-box-content-holder">
            <div class="sidebar-box-content">
                <div class="sidebar-gallery-block">
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-1.jpg"/>
                        </div>
                    </div>
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-2.jpg"/>
                        </div>
                    </div>
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-3.jpg"/>
                        </div>
                    </div>
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-4.jpg"/>
                        </div>
                    </div>
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-5.jpg"/>
                        </div>
                    </div>
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-6.jpg"/>
                        </div>
                    </div>
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-7.jpg"/>
                        </div>
                    </div><div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-1.jpg"/>
                        </div>
                    </div>
                    <div class="sidebar-gallery-item">
                        <div class="sidebar-gallery-img-container">
                            <img class="sidebar-gallery-img" src="http://localhost/findyo/uploads/sample/gallery-img-2.jpg"/>
                        </div>
                    </div>
                </div>
                <div class="sidebar-view-more-block">
                    <a href="#">View gallery</a>
                </div>
            </div>
        </div>
        <!--sidebar content holder ends-->

    </div>
    <div class="white-box" id="calendar_card">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title"><span id="user-first-name">Gayan</span>'s calendar</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--sidebar content holder begins-->
        <div class="sidebar-box-content-holder">
            <div class="sidebar-box-content">
                <div class="sidebar-calendar-block">
                    <div class="sidebar-calendar" id="sidebar-calendar"></div>
                </div>
            </div>
        </div>
        <!--sidebar content holder ends-->
        <script>
            $(document).ready(function () {
                var eventData = [
                    {"date": "2017-12-21", "badge": true, "title": "Booked"},
                    {"date": "2017-12-26", "badge": true, "title": "Busy"}
                ];
                $("#sidebar-calendar").zabuto_calendar({
                    data: eventData,
                    cell_border: true,
                    today: true,
                    show_days: false,
                    weekstartson: 0,
                    nav_icon: {
                        prev: '<i class="fa fa-chevron-left"></i>',
                        next: '<i class="fa fa-chevron-right"></i>'
                    },
                    ajax: {
                        url: "show_data.php",
                        modal: true
                    }
                });
            });
        </script>

    </div>
    <div class="white-box" id="top_rated_projects_card">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title">Top rated projects</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--sidebar content holder begins-->
        <div class="sidebar-box-content-holder">
            <div class="sidebar-box-content">
                <div class="sidebar-top-projects-block">
                    <ul>
                        <li>
                            <div class="project-thumbnail" style="background-image: url(http://localhost/findyo/uploads/sample/project-1.png);"></div>
                            <div class="sidebar-project-content-holder">
                                <p class="sidebar-project-title">Efficiency</p>
                                <div class="rating-block">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <p class="sidebar-content-desc project-desc">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="project-thumbnail" style="background-image: url(http://localhost/findyo/uploads/sample/project-2.jpg);"></div>
                            <div class="sidebar-project-content-holder">
                                <p class="sidebar-project-title">HRM</p>
                                <div class="rating-block">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half" aria-hidden="true"></i>
                                </div>
                                <p class="sidebar-content-desc project-desc">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="project-thumbnail" style="background-image: url(http://localhost/findyo/uploads/sample/project-3.jpg);"></div>
                            <div class="sidebar-project-content-holder">
                                <p class="sidebar-project-title">Dashboard</p>
                                <div class="rating-block">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half" aria-hidden="true"></i>
                                </div>
                                <p class="sidebar-content-desc project-desc">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="project-thumbnail" style="background-image: url(http://localhost/findyo/uploads/sample/project-1.png);"></div>
                            <div class="sidebar-project-content-holder">
                                <p class="sidebar-project-title">User profile</p>
                                <div class="rating-block">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <p class="sidebar-content-desc project-desc">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-view-more-block">
                    <a href="#">View all projects</a>
                </div>
            </div>
        </div>
        <!--sidebar content holder ends-->

    </div>
</div>
