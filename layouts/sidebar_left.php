<div class="profle-left">
    <div class="white-box" id="profile_summery_card">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title">Profile summery</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--title bar ends-->
        <!--sidebar content holder begins-->
        <div class="sidebar-box-content-holder">
            <div class="sidebar-box-content">
                <div class="box-title-block">
                    <h3 class="box-title">About me</h3>
                </div>
                <div class="sidebar-content-desc-block">
                    <p class="sidebar-content-desc">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
                    </p>
                </div>
            </div>
            <div class="sidebar-box-content">
                <div class="box-title-block">
                    <h3 class="box-title">Skills & Endorsements</h3>
                </div>
                <div class="sidebar-content-desc-block">
                    <ul class="skills-ul">
                        <li>
                            <a class="material-btn-click-md">
                                #<span class="skill-tag-name">Web design</span>
                                <span class="skill-endorsement-count">52</span>
                            </a>
                        </li>
                        <li>
                            <a class="material-btn-click-md">
                                #<span class="skill-tag-name">Photoshop</span>
                                <span class="skill-endorsement-count">52</span>
                            </a>
                        </li>
                        <li>
                            <a class="material-btn-click-md">
                                #<span class="skill-tag-name">User Experience</span>
                                <span class="skill-endorsement-count">52</span>
                            </a>
                        </li>
                        <li>
                            <a class="material-btn-click-md">
                                #<span class="skill-tag-name">HTML</span>
                                <span class="skill-endorsement-count">52</span>
                            </a>
                        </li>
                        <li>
                            <a class="material-btn-click-md">
                                #<span class="skill-tag-name">CSS</span>
                                <span class="skill-endorsement-count">52</span>
                            </a>
                        </li>
                        <li>
                            <a class="material-btn-click-md">
                                #<span class="skill-tag-name">JavaScript</span>
                                <span class="skill-endorsement-count">52</span>
                            </a>
                        </li>
                    </ul>
                    <div class="sidebar-view-more-block">
                        <a href="#">View <span class="numbers-of-endorsements">21</span> more skills<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="sidebar-box-content">
                <div class="box-title-block">
                    <h3 class="box-title">Experiences</h3>
                </div>
                <div class="sidebar-content-desc-block">
                    <ul class="experience-ul">
                        <li>
                            <a href="#">
                                <p>UI/UX Engineer at Suwahas Holdings <span class="experience-time">(2017 -  Current)</span></p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <p>UI Engineer(Jr) at Quard International <span class="experience-time">(2016 - 2017) - 1 year</span></p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <p>UI Engineer(Jr) at Quard International  <span class="experience-time">(2016 - 2017) - 1 year</span></p>
                            </a>
                        </li>
                    </ul>
                    <div class="sidebar-view-more-block">
                        <a href="#">View <span class="numbers-of-endorsements">2</span> more experiences<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            <div class="sidebar-box-content">
                <div class="box-title-block">
                    <h3 class="box-title">Professional interests</h3>
                </div>
                <div class="sidebar-content-desc-block">
                    <ul class="professional-interests-ul">
                        <li><a href="#">#Lorem_ipsum</a></li>
                        <li><a href="#">#LoremIpsum</a></li>
                        <li><a href="#">#Loremipsum</a></li>
                        <li><a href="#">#Lorem_ipsum</a></li>
                        <li><a href="#">#Lorem_ipsum</a></li>
                    </ul>
                    <div class="sidebar-view-more-block">
                        <a href="#">View <span class="numbers-of-prof-interests">5</span> more interests<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            <div class="sidebar-box-content">
                <div class="box-title-block">
                    <h3 class="box-title">Share this profile</h3>
                </div>
                <div class="sidebar-content-desc-block">
                    <ul class="share-profile-ul">
                        <li><a class="fb" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a class="gplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--sidebar content holder ends-->

    </div>
    <div class="white-box" id="reviews_card">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title">Client reviews</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--title bar ends-->
        <!--sidebar content holder begins-->
        <div class="sidebar-box-content-holder">
            <div class="sidebar-box-content">
                <div class="sidebar-content-desc-block">
                    <ul class="professional-interests-ul">
                        <li>
                            <div class="reviewer user-block">
                                <div class="user-img-block">
                                    <div class="user-img" style="background-image: url(http://localhost/findyo/uploads/sample/pro_pic_01.png);"></div>
                                </div>
                                <div class="user-post-right">
                                    <div class="user-published-block">
                                        <p>"<span class="review-short">Fast service</span>"</p>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-content-desc-block">
                                <p class="sidebar-content-desc">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                <div class="rating-block">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <div class="reviewer-name">
                                    <p>-<span>John lehgnan</span>-</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="reviewer user-block">
                                <div class="user-img-block">
                                    <div class="user-img" style="background-image: url(http://localhost/findyo/uploads/sample/pro_pic_01.png);"></div>
                                </div>
                                <div class="user-post-right">
                                    <div class="user-published-block">
                                        <p>"<span class="review-short">Superb quality</span>"</p>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-content-desc-block">
                                <p class="sidebar-content-desc">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                <div class="rating-block">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half" aria-hidden="true"></i>
                                </div>
                                <div class="reviewer-name">
                                    <p>-<span>John lehgnan</span>-</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="reviewer user-block">
                                <div class="user-img-block">
                                    <div class="user-img" style="background-image: url(http://localhost/findyo/uploads/sample/pro_pic_01.png);"></div>
                                </div>
                                <div class="user-post-right">
                                    <div class="user-published-block">
                                        <p>"<span class="review-short">Helped me in many w...</span>"</p>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-content-desc-block">
                                <p class="sidebar-content-desc">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                <div class="rating-block">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half" aria-hidden="true"></i>
                                </div>
                                <div class="reviewer-name">
                                    <p>-<span>John lehgnan</span>-</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="sidebar-view-more-block">
                        <a href="#">View <span class="numbers-of-prof-interests">2</span> more reviews<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!--sidebar content holder ends-->

    </div>
</div>