<?php

function top_services_home() {
    ?>
    <div class="col-md-3 col-sm-4 col-xs-6 box-loop-item">
        <div class="perspective-item">
            <div class="rotate-item">
                <a href="#">
                    <div class="item-holder">
                        <!--title bar begins-->
                        <div class="top-services-title-block">
                            <div class="freelancer-block">
                                <div class="freelancer-img-block">
                                    <div class="freelancer-img" style="background-image: url(http://localhost/findyo/uploads/sample/gayan.jpg);"></div>
                                </div>
                                <div class="freelancer-title-block">
                                    <div class="freelancer-name">Gayan sandamal</div>
                                    <div class="freelancer-title">ux engineer</div>
                                </div>
                            </div>
                        </div>
                        <!--title bar ends-->
                        <div class="item-img-block services" style="background-image: url(http://localhost/findyo/uploads/sample/photo.jpg);"></div>
                        <div class="service-details-block">
                            <div class="service-description-block">
                                <h5 class="service-descrition">I will take 50 photographs
                                    <span class="service-price-block">for Rs.<span class="service-price">3,200</span></span>
                                </h5>
                            </div>
                            <div class="service-rate-block">
                                <img class="rate-icon" src="http://localhost/findyo/assets/img/icons/rate.png"/>
                                <span class="rate-value">96.66%</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
<?php } ?>