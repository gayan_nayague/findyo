<?php

function top_freelancers_home() {
    ?>
    <div class="col-md-3 col-sm-4 col-xs-6 box-loop-item">
        <a href="#">
            <div class="home-freelancer-block">
                <div class="freelancer-block">
                    <div class="freelancer-img-block">
                        <div class="freelancer-img" style="background-image: url(http://localhost/findyo/uploads/sample/gayan.jpg);"></div>
                    </div>
                    <div class="freelancer-title-block">
                        <div class="freelancer-name">Gayan sandamal</div>
                        <div class="freelancer-title">ux engineer</div>
                    </div>

                    <div class="service-rate-container">
                        <div class="service-rate-block">
                            <img class="rate-icon" src="http://localhost/findyo/assets/img/icons/rate.png"/>
                            <span class="rate-value">96.66%</span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
<?php } ?>