<?php

function top_categories_home() {
    ?>
    <div class="col-md-3 col-sm-4 col-xs-6 box-loop-item">
        <div class="perspective-item">
            <div class="rotate-item"> 
                <a href="#">
                    <div class="item-holder">
                        <!--title bar begins-->
                        <div class="top-category-title-block">
                            <h3 class="top-category-title">Photographers
                                <span class="category-item-count-block">
                                    (<span class="category-item-count">207</span>)
                                </span>
                            </h3>
                        </div>
                        <!--title bar ends-->
                        <div class="item-img-block" style="background-image: url(http://localhost/findyo/uploads/sample/photographer.jpg);">
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
<?php } ?>