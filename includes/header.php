<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Findyo.lk</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $base_url = "http://" . $_SERVER['SERVER_NAME'] . '/findyo'; ?>
        <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/css/bootstrap3.3.7.min.css">
        <script src="<?php echo $base_url; ?>/assets/js/jquery3.2.1.min.js"></script>
        
        <!--http://zabuto.com/dev/calendar/examples/-->
        <script src="<?php echo $base_url; ?>/assets/js/zabuto_calendar.min.js"></script>
        <script src="<?php echo $base_url; ?>/assets/css/zabuto_calendar.min.css"></script>

        <!--Google fonts begins-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=El+Messiri:400,500,600,700" rel="stylesheet">
        <!--Google fonts ends-->
        <!--Fontawesome begins-->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <!--Fontawesome ends-->

        <!--custom styles begins-->
        <link rel="stylesheet" href="<?php echo $base_url; ?>/assets/css/style.css"/>
        <!--custom styles ends-->
    <body>
        <!--Header begins-->
        <div class="full-width header-outer-block">
            <div class="container no-side-paddings">
                <div class="header-inner-block">
                    <!--logo block begins-->
                    <div class="logo-block">
                        <a href="index.php" class="logo-link">
                            <div class="logo-img-block">
                                <img class="logo-img" src="<?php echo $base_url; ?>/assets/img/logo.png"/>
                            </div>
                            <div class="logo-text-block">
                                <span class="logo-name">Findyo.lk</span>
                                <span class="logo-tagline">Find your own way</span>
                            </div>
                        </a>
                    </div>
                    <!--logo block ends-->
                    <!--search block begins-->
                    <div class="top-search-block">
                        <input type="text" placeholder="Type here to search" class="top-search-field"/>
                        <a href="#" class="top-search-button">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!--search block ends-->

                    <div class="header-right-content-block">
                        <?php // include_once 'includes/header-menu/login-buttons.php'; ?>
                        <?php include_once 'includes/header-menu/current-user-details.php'; ?>
                    </div>
                </div>
            </div>
        </div>
        <!--Header ends-->
