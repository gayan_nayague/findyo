<div class="banner-content-container register-block">
    <div class="banner-form-block">
        <div class="back-to-init-block">
            <a href="#" class="back-to-init-btn"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
        </div>
        <div class="banner-form-title-block">
            <h3 class="banner-form-title">Register</h3>
        </div>

        <div class="banner-form-field-container">
            <div class="button-container-stack">
                <input class="input-field" type="text" placeholder="Type your email here"/>
            </div>
            <div class="button-container-stack">
                <input class="input-field" type="password" placeholder="Type your password here"/>
            </div>
            <div class="button-container-stack">
                <input class="input-field" type="password" placeholder="Re-type your password"/>
            </div>
            <div class="button-container-stack">
                <input class="input-checkbox ui-checkbox" id="checkbox-1-1" type="checkbox"/>
                <label for="checkbox-1-1"><span>I agree with the <a  href="#x">Terms and conditions</a></span></label>
            </div>
            <div class="button-container-stack">
                <a class="main-btn red-btn material-btn-click-lg" href="#">Register</a>
            </div>
        </div>
        <div class="login-additional-block">
            <a href="#" id="already_have_an_account">Already have an account? Login</a>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.material-btn-click-md').click(function () {
            $(this).addClass('after');
            setTimeout(function () {
                $('.material-btn-click-md').removeClass('after');
            }, 500);
        });
        $('.material-btn-click-lg').click(function () {
            $(this).addClass('after');
            setTimeout(function () {
                $('.material-btn-click-lg').removeClass('after');
            }, 1000);
        });
        $('.back-to-init-btn').on('click', (event) => {
            event.preventDefault();
            go_to_init();
        });
//    home looking for a job button begins
        $('#already_have_an_account').on('click', (event) => {
            event.preventDefault();
            go_to_login();
        });
//    home looking for a job button ends
    });
</script>