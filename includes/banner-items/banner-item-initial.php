<div class="banner-content-container init">
    <div class="banner-heading-block" id="banner-heading-block">
        <h1 class="banner-heading">
            <span class="line-breaker">Sri Lanka’s first and best</span><span class="line-breaker">online market place to find services</span><span class="line-breaker">from anywhere at anytime</span>
        </h1>
    </div>
    <div class="banner-main-direct-button-block">
        <div class="button-container-inline">
            <a class="main-btn green-btn material-btn-click-md" id="looking_for_a_service_btn" href="#">I'm looking for a service</a>
        </div>
        <div class="button-container-inline">
            <a class="main-btn green-btn material-btn-click-md" id="looking_for_a_job_btn" href="#">I'm looking for a job</a>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.material-btn-click-md').click(function () {
            $(this).addClass('after');
            setTimeout(function () {
                $('.material-btn-click-md').removeClass('after');
            }, 500);
        });
        $('.material-btn-click-lg').click(function () {
            $(this).addClass('after');
            setTimeout(function () {
                $('.material-btn-click-lg').removeClass('after');
            }, 1000);
        });

//    home looking for a service button begins
        $('#looking_for_a_service_btn').on('click', (event) => {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("#home_categories").offset().top - 100
            }, 1000);
        });
//    home looking for a service button ends
//    home looking for a job button begins
        $('#looking_for_a_job_btn').on('click', (event) => {
            event.preventDefault();
            go_to_login();
        });
//    home looking for a job button ends
    });
</script>