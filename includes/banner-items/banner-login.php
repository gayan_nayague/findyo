<div class="banner-content-container login-block">
    <div class="banner-form-block">
        <div class="back-to-init-block">
            <a href="#" class="back-to-init-btn"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
        </div>
        <div class="banner-form-title-block">
            <h3 class="banner-form-title">Login</h3>
        </div>
        <div class="banner-form-field-container">
            <div class="button-container-stack">
                <a class="main-btn fb-login-btn material-btn-click-lg" href="#"><i class="fa fa-facebook" aria-hidden="true"></i> Login with Facebook</a>
            </div>
            <div class="button-container-stack">
                <a class="main-btn google-login-btn material-btn-click-lg" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Login with Google+</a>
            </div>
        </div>

        <div class="login-separator-block">
            <hr class="login-separator"/>
            <span class="login-separator-text">or</span>
            <hr class="login-separator"/>
        </div>

        <div class="banner-form-field-container">
            <div class="button-container-stack">
                <input class="input-field" type="text" placeholder="Type your username or email here"/>
            </div>
            <div class="button-container-stack">
                <input class="input-field" type="password" placeholder="Type your password here"/>
            </div>
            <div class="button-container-stack">
                <a class="main-btn green-btn material-btn-click-lg" href="#">Login</a>
            </div>
        </div>
        <div class="login-additional-block">
            <a href="#">Forgot your password?</a>
            <a href="#" id="new_sign_up">New? Sign up</a>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.material-btn-click-md').click(function () {
            $(this).addClass('after');
            setTimeout(function () {
                $('.material-btn-click-md').removeClass('after');
            }, 500);
        });
        $('.material-btn-click-lg').click(function () {
            $(this).addClass('after');
            setTimeout(function () {
                $('.material-btn-click-lg').removeClass('after');
            }, 1000);
        });
        $('.back-to-init-btn').on('click', (event) => {
            event.preventDefault();
            go_to_init();
        });
        $('#new_sign_up').on('click', (event) => {
            event.preventDefault();
            go_to_register();
        });
    });
</script>