<div class="banner-block">
    <div class="banner-img-block">
        <div class="banner-img" style="background-image: url(<?php echo $base_url; ?>/assets/img/home-banner/home-banner-1.jpg);">
        </div>
        <div class="banner-gradient"></div>
    </div>
    <div class="banner-content-block">
        <div class="banner-content-area">
            <div class="banner-dynamic-content-block">
                <?php include 'banner-items/banner-item-initial.php'; ?>
                <?php // include 'banner-items/banner-login.php'; ?>
                <?php // include 'banner-items/banner-register.php'; ?>
            </div>
        </div>
    </div>
</div>