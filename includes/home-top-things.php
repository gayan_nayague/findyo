<div class="container no-side-paddings">

    <!--top selling categories begins-->
    <div class="col-sm-12 no-side-paddings white-box" id="home_categories">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title">Top selling categories</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--title bar ends-->
        <div class="box-loop-block">
            <?php include_once 'layouts/loops/top-categories-loop.php'; ?>
            <?php
            $top_categories_home_count = 12;
            for ($x = 0; $x <= $top_categories_home_count - 1; $x++) {
                top_categories_home();
            }
            ?>
            <div class="col-sm-12 view-all-block">
                <div class="view-all-holder">
                    <a href="#">View all categories<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

    </div>
    <!--top selling categories ends-->

    <!--top selling services begins-->
    <div class="col-sm-12 no-side-paddings white-box">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title">Top selling services</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--title bar ends-->
        <div class="box-loop-block">
            <?php include_once 'layouts/loops/top-services-loop.php'; ?>
            <?php
            $top_services_home_count = 12;
            for ($x = 0; $x <= $top_services_home_count - 1; $x++) {
                top_services_home();
            }
            ?>
            <div class="col-sm-12 view-all-block">
                <div class="view-all-holder">
                    <a href="#">View all categories<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

    </div>
    <!--top selling services ends-->

    <!--top freelancers begins-->
    <div class="col-sm-12 no-side-paddings white-box">
        <!--title bar begins-->
        <div class="box-title-bar">
            <div class="box-title-block">
                <h3 class="box-title">Top rated freelancers</h3>
            </div>
            <div class="box-button-block">
                <div class="box-button-container">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <!--title bar ends-->
        <div class="box-loop-block">
            <?php include_once 'layouts/loops/top-freelancers-loop.php'; ?>
            <?php
            $top_freelancers_home_count = 12;
            for ($x = 0; $x <= $top_freelancers_home_count - 1; $x++) {
                top_freelancers_home();
            }
            ?>
            <div class="col-sm-12 view-all-block">
                <div class="view-all-holder">
                    <a href="#">View all freelancers<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

    </div>
    <!--top freelancers ends-->

</div>