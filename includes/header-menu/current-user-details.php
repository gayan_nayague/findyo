<div class="header-controllers-block">
    <div class="requests-block">
        <i class="fa fa-users" aria-hidden="true"></i>
        <div class="requests-count-block"><span class="requests-count">5</span></div>
    </div>
    <div class="chats-block">
        <i class="fa fa-comments-o" aria-hidden="true"></i>
        <div class="chats-count-block"><span class="chats-count">21</span></div>
    </div>
    <div class="notifications-block">
        <i class="fa fa-bell" aria-hidden="true"></i>
        <div class="notifications-count-block"><span class="notifications-count">10</span></div>
    </div>
</div>

<div class="freelancer-block current-user">
    <a href="profile.php">
        <div class="freelancer-img-block">
            <div class="freelancer-img" style="background-image: url(http://localhost/findyo/uploads/sample/gayan.jpg);"></div>
        </div>
        <div class="freelancer-title-block">
            <div class="freelancer-name">Gayan sandamal</div>
            <div class="freelancer-title">ux engineer</div>
        </div>
        <div class="user-more-click-block">
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </div>
    </a>
</div>