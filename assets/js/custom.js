function go_to_init() {
    $('.banner-content-container.login-block').addClass('go-login');
    $('.banner-content-container.register-block').addClass('go-register');
    setTimeout(function () {
        $('.banner-content-container.login-block').remove();
        $('.banner-content-container.register-block').remove();
        $('.banner-dynamic-content-block').load('http://localhost/findyo/includes/banner-items/banner-item-initial.php', function () {
            setTimeout(function () {
                $('.banner-content-container.init').addClass('come-init');
            }, 500);
        });
    }, 500);
}
function go_to_login() {
    $('.banner-content-container.init').addClass('go-init');
    $('.banner-content-container.register-block').addClass('go-register');
    setTimeout(function () {
        $('.banner-content-container.init').remove();
        $('.banner-dynamic-content-block').load('http://localhost/findyo/includes/banner-items/banner-login.php', function () {
            setTimeout(function () {
                $('.banner-content-container.login-block').addClass('come-login');
            }, 500);
        });
    }, 500);
}
function go_to_register() {
    $('.banner-content-container.init').addClass('go-init');
    $('.banner-content-container.login-block').addClass('go-login');
    setTimeout(function () {
        $('.banner-content-container.init').remove();
        $('.banner-content-container.login-block').remove();
        $('.banner-dynamic-content-block').load('http://localhost/findyo/includes/banner-items/banner-register.php', function () {
            setTimeout(function () {
                $('.banner-content-container.register-block').addClass('come-register');
            }, 500);
        });
    }, 500);
}


$(document).ready(function () {
    $('.material-btn-click-md').click(function () {
        $(this).addClass('after');
        setTimeout(function () {
            $('.material-btn-click-md').removeClass('after');
        }, 500);
    });
    $('.material-btn-click-lg').click(function () {
        $(this).addClass('after');
        setTimeout(function () {
            $('.material-btn-click-lg').removeClass('after');
        }, 1000);
    });
//header login, register buttons begins
    $('#header_login_btn').on('click', (event) => {
        event.preventDefault();
        go_to_login();
    });
    $('#header_register_btn').on('click', (event) => {
        event.preventDefault();
        go_to_register();
    });
    $('.back-to-init-btn').on('click', (event) => {
        alert('qw');
        event.preventDefault();
        go_to_init();
    });
    go_to_init();
//header login, register buttons ends

//timeline filter
    $('.filter-by-toggle').on('click', () => {
        $('.filter-items-block').toggleClass('hide');
    });
    $('.filter-items-block > ul > li').on('click', (e) => {
        $('.filter-items-block > ul > li').removeClass('active');
        $(e.currentTarget).addClass('active');
    });
    $('.filter-items-block > ul > li > span.orderby-icon-block').on('click', (e) => {
        $(e.currentTarget).find('i').toggleClass('hide');
    });
//timeline filter

});